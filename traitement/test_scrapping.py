from joblib import load
import pandas as pd
import random


df=pd.read_json("scrapping/scraped_data.json")

#Génere une liste de 400 indices aléatoires
random_list = random.sample(range(1,len(df)-1), 400)

#Notation manuelle de 400 tweets
i=0
for x in random_list:
    print(df["content"][x])
    df.at[x,"sentiment"]=int(input(f'Tweet {i} :'))
    i+=1
    df.to_json('scrapping/scrapped_data_sentiments.json')

