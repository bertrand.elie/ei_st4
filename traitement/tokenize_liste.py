import spacy
from nltk.stem import WordNetLemmatizer

# Chargement du modèle de langue française de SpaCy
nlp = spacy.load("fr_core_news_sm")

# Liste des chemins de fichiers à traiter
paths = ['negatifs', 'positifs']
for path in paths:
    for lang in ["fr","en"]:
        # Définition du chemin d'entrée du fichier
        file_path = f'./Liste_mots/mots_{path}_{lang}.txt'

        # Lecture du contenu du fichier
        with open(file_path, 'r', encoding='utf-8') as file:
            text = file.read()
        if lang=='fr':
            # Tokenization du texte et lemmatisation en utilisant SpaCy
            doc = nlp(text)
            processed_tokens_fr= [token.lemma_.lower() for token in doc]
        else :
            wl = WordNetLemmatizer()
            words=text.splitlines()
            tokens=[]
            for i in range(len(words)):
                tokens.append(words[i])
                tokens.append('\n')
            processed_tokens_en=[wl.lemmatize(token).lower() for token in tokens]
            
    processed_tokens= processed_tokens_en + processed_tokens_fr
    # Définition du chemin de sortie du fichier
    output_file_path = f'./Liste_mots/processed_{path}.txt'
        
    # Écriture des tokens traités dans le fichier de sortie
    with open(output_file_path, 'w', encoding='utf-8') as output_file:
        output_file.write(''.join(processed_tokens))  # Utiliser join sans espace entre les tokens

print("Le traitement des fichiers est terminé.")
