<<<<<<< HEAD
from tqdm import tqdm
import spacy
import re
from nltk.stem import WordNetLemmatizer
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
import pandas as pd
import nltk
nltk.download('stopwords')
nltk.download('punkt')
# Définir une fonction pour tokeniser et filtrer le texte


=======
import pandas as pd
import nltk
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
import re
import spacy
from tqdm import tqdm

# Définir une fonction pour tokeniser et filtrer le texte
>>>>>>> main
def tokenize_and_filter(text, lang):
    # Supprimer les URLs du texte et convertir en minuscules
    regex_url = r"https?://\S+"
    text_without_url = re.sub(regex_url, '', text).lower()
<<<<<<< HEAD

=======
    
>>>>>>> main
    # Obtenir les stopwords pour la langue spécifiée
    stopwords_set = set(stopwords.words(lang))

    # Tokeniser le texte
    words = word_tokenize(text_without_url)
    words_filtered = []

    # Si la langue est le français, exclure certains mots de négation des stopwords
    if lang == 'french':
<<<<<<< HEAD
        mots_negations = ["oui", "non", "ne", "ni", "mais", "jamais", "que", "guère",
                          "point", "personne", "rien", "aucun", "personne", "bien", "si", "même"]
        stopwords_set = stopwords_set.difference(mots_negations)

=======
        mots_negations = ["oui", "non", "ne", "ni", "mais", "jamais", "que", "guère", "point", "personne", "rien", "aucun", "personne", "bien", "si", "même"]
        stopwords_set = stopwords_set.difference(mots_negations)
    
>>>>>>> main
    # Ajouter 'via' aux stopwords pour toutes les langues
    stopwords_set.add('via')
    stopwords_set.add('rt')

    # Filtrer les stopwords et les tokens non alphanumériques
    for w in words:
        if w not in stopwords_set and w.isalnum():
            words_filtered.append(w)
<<<<<<< HEAD

    return words_filtered

# Définir une fonction pour lemmatiser les tokens en anglais


def lemma_en(tokens):
    wl = WordNetLemmatizer()
    lemma_words_of_tokens = []

    # Lemmatizer chaque token
    for i in tokens:
        lemma_words_of_tokens.append(wl.lemmatize(i))

    return lemma_words_of_tokens

# Définir une fonction pour lemmatiser les tokens en français en utilisant spaCy


def lemma_fr(tokens, nlp):
    # Créer un document spaCy à partir des tokens
    doc = nlp(" ".join(tokens))

=======
    
    return words_filtered

# Définir une fonction pour lemmatiser les tokens en anglais
def lemma_en(tokens):
    wl = WordNetLemmatizer()
    lemma_words_of_tokens = []
    
    # Lemmatizer chaque token
    for i in tokens:
        lemma_words_of_tokens.append(wl.lemmatize(i))
    
    return lemma_words_of_tokens

# Définir une fonction pour lemmatiser les tokens en français en utilisant spaCy
def lemma_fr(tokens, nlp):
    # Créer un document spaCy à partir des tokens
    doc = nlp(" ".join(tokens))
    
>>>>>>> main
    # Extraire les tokens lemmatisés du document
    return [token.lemma_ for token in doc]

# Définir la fonction principale pour traiter le dataset
<<<<<<< HEAD


def main():
    # Charger le modèle spaCy pour le français
    nlp = spacy.load("fr_core_news_sm")

    # Lire le dataset à partir d'un fichier JSON
    df = pd.read_json("./Corpus/Full_DataSet_v3.json")
    # Enlever les RT
    df = df[~df['content'].str.startswith("RT")].reset_index(drop=True)

    # Séparer le dataset en sous-ensembles anglais et français
    df_en = df[df["langue"] == "en"].reset_index(drop=True)
    df_fr = df[df["langue"] == "fr"].reset_index(drop=True)

    # Traiter le sous-ensemble anglais
    n = len(df_en)
    content_processed = []
    for i in tqdm(range(n)):
        if df_en["content"][i].split()[0] != "RT":
            text = df_en["content"][i]
            tokens = tokenize_and_filter(text, "english")
            lemma_words_of_tokens = lemma_en(tokens)
            content_processed.append(lemma_words_of_tokens)
    df_en["content_processed"] = content_processed

    # Sauvegarder le sous-ensemble anglais traité dans un fichier JSON
    df_en.to_json('./Corpus/Full_DataSet_en_no_rt.json')

    # Traiter le sous-ensemble français
    n = len(df_fr)
    content_processed = []
    for i in tqdm(range(n)):
        if df_fr["content"][i].split()[0] != "RT":
            text = df_fr["content"][i]
            tokens = tokenize_and_filter(text, "french")
            lemma_words_of_tokens = lemma_fr(tokens, nlp)
            content_processed.append(lemma_words_of_tokens)
    df_fr["content_processed"] = content_processed

    # Sauvegarder le sous-ensemble français traité dans un fichier JSON
    df_fr.to_json('./Corpus/Full_DataSet_fr_no_rt.json')

=======
def main():
    # Charger le modèle spaCy pour le français
    nlp = spacy.load("fr_core_news_sm")
    
    # Lire le dataset à partir d'un fichier JSON
    df_fr=pd.read_json("scrapping/scrapped_data_sentiments.json")
    
    #Traite le dataset
    n = len(df_fr)
    content_processed = []
    for i in tqdm(range(n)):
        
        text = df_fr["content"][i]
        tokens = tokenize_and_filter(text, "french")
        lemma_words_of_tokens = lemma_fr(tokens, nlp)
        content_processed.append(lemma_words_of_tokens)
    df_fr["content_processed"] = content_processed
    
    # Sauvegarder le dataframe traité dans un fichier JSON
    df_fr.to_json('./scrapping/scrapped_data_sentiments.json')
>>>>>>> main

# Exécuter la fonction principale
main()
