import pandas as pd
from collections import Counter
from nltk import bigrams
import numpy as np

# Lecture des mots positifs depuis le fichier et création d'un ensemble
with open('scrapping/scrapped_words_pos.txt', 'r', encoding='utf-8') as file:
    mots_positifs = file.read()
mots_positifs = set(mots_positifs.split(sep='\n')[:-1])

# Lecture des mots négatifs depuis le fichier et création d'un ensemble
with open('scrapping/scrapped_words_neg.txt', 'r', encoding='utf-8') as file:
    mots_negatifs = file.read()
mots_negatifs = set(mots_negatifs.split(sep='\n')[:-1])

# Initialisation d'un dictionnaire pour stocker les polarités des mots
mots_polarisants = {}
# Initialisation d'un dictionnaire pour stocker les polarités des bigrammes
bigram_polarisants = {}

# Attribution de la polarité positive aux mots positifs et négative aux mots négatifs
for mot in mots_positifs :
    if mot not in mots_negatifs:
        mots_polarisants[mot] = 1
    
for mot in mots_negatifs:
    if mot not in mots_positifs:
        mots_polarisants[mot] = -1

# Fonction pour calculer le score d'un texte en fonction de la polarité des mots et des bigrammes
def score(tokens):
    s = 0
    # Calcul du score en fonction de la polarité des mots
    for token in tokens:
        if token in mots_polarisants:
            s += mots_polarisants[token]
    # Calcul du score en fonction de la polarité des bigrammes
    for bigram in list(bigrams(tokens)):
        if bigram in bigram_polarisants:
            s += bigram_polarisants[bigram]
    return s

def main():
    # Parcourir les données pour chaque langue
    for lg in ['fr']:
        # Lecture du DataFrame à partir du fichier JSON
        df = pd.read_json(f"scrapping/scrapped_data_sentiments.json")
        n = len(df)
        # Initialisation de la colonne 'score_comptage'
        df['score_comptage'] = 0
        
        index = set(df[~df['sentiment'].apply(np.isnan)].index)
        
        # Répéter le processus deux fois pour inclure les bigrammes
        for i in range(3):    
            score_comptage = []
            # Calcul du score pour chaque texte
            for i in range(n):
                if i in index:
                    score_comptage.append(df["sentiment"][i])
                else:
                    s = score(df["content_processed"][i])
                    score_comptage.append(s)
       
            df['sentiment'] = np.sign(score_comptage)
                    
            # Séparation des tokens en fonction du score de comptage
            negative_tokens = df[df['sentiment'] < 0]['content_processed']
            positive_tokens = df[df['sentiment'] > 0]['content_processed']
            
            # Initialisation des compteurs de bigrammes pour les textes négatifs et positifs
            bigram_counter_negative = Counter()
            bigram_counter_positive = Counter()

            # Compter les bigrammes dans les textes négatifs
            for tokens in negative_tokens:
                bigram_list = list(bigrams(tokens))
                bigram_counter_negative.update(bigram_list)

            # Compter les bigrammes dans les textes positifs
            for tokens in positive_tokens:
                bigram_list = list(bigrams(tokens))
                bigram_counter_positive.update(bigram_list)

            # Trouver les 20 bigrammes les plus communs dans les textes négatifs et positifs
            most_common_negative_bigrams = bigram_counter_negative.most_common(30)
            most_common_positive_bigrams = bigram_counter_positive.most_common(30)

            # Attribuer la polarité aux bigrammes les plus communs
            for bigram, _ in most_common_negative_bigrams:
                bigram_polarisants[bigram] = -3
                
            for bigram, _ in most_common_positive_bigrams:
                bigram_polarisants[bigram] = 3
        
                
        # Écriture du DataFrame mis à jour dans un nouveau fichier JSON
        df.to_json(f"scrapping/scrapped_data_sentiments_v2.json")
        
# Exécuter la fonction principale
main()
