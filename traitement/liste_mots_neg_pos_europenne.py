import pandas as pd
import random
from collections import Counter
from numpy import isnan

# Charger le DataFrame à partir du fichier JSON
# Le fichier JSON contient les données nécessaires pour l'analyse
df = pd.read_json('scrapping/scrapped_data_sentiments.json')


# Sélectionner les 400 tweets notés
df=df[~df["sentiment"].apply(isnan)]



# Séparer les contenus en listes positives et négatives en fonction de la polarité
# 1 pour les contenus positifs et -1 pour les contenus négatifs
content_positive = df[df['sentiment'] ==1]["content_processed"].tolist()
content_negative = df[df['sentiment'] == -1]["content_processed"].tolist()

# Fonction pour compter les mots les plus communs dans une liste de listes de tokens
def most_common_words(token_lists):
    words = []
    for tokens in token_lists:
        # Étendre la liste des mots avec les tokens
        words.extend(tokens)
    # Retourner les 150 mots les plus communs
    return Counter(words).most_common(150)

# Obtenir les mots les plus communs pour les contenus positifs et négatifs
common_words_positive = most_common_words(content_positive)
common_words_negative = most_common_words(content_negative)

# Fichier pour enregistrer les résultats des mots les plus communs dans les contenus positifs
output_file_positive = 'scrapping/scrapped_words_pos.txt'

# Ouvrir le fichier en mode écriture et écrire les mots les plus communs dans les contenus positifs
with open(output_file_positive, 'w') as f:
    for word, count in common_words_positive:
        f.write(f"{word}\n")

print(f"Les mots les plus communs positifs ont été enregistrés dans {output_file_positive}.")

# Fichier pour enregistrer les résultats des mots les plus communs dans les contenus négatifs
output_file_negative = 'scrapping/scrapped_words_neg.txt'

# Ouvrir le fichier en mode écriture et écrire les mots les plus communs dans les contenus négatifs
with open(output_file_negative, 'w') as f:
    for word, count in common_words_negative:
        f.write(f"{word}\n")

print(f"Les mots les plus communs négatifs ont été enregistrés dans {output_file_negative}.")
