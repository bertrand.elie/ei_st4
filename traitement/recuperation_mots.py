import pandas as pd
import random
from collections import Counter

# Charger le DataFrame à partir du fichier JSON
# Le fichier JSON contient les données nécessaires pour l'analyse
df = pd.read_json('Corpus/Full_DataSet_fr_no_rt.json')

# Ajouter une nouvelle colonne 'polarite' avec des valeurs manquantes (NaN)
# Cette colonne servira à stocker la polarité des contenus
df["polarite"] = pd.NA

# Sélectionner de manière aléatoire 200 éléments parmi les documents spécifiés et réinitialiser les index
# Les documents sélectionnés sont "randomtweets1", "randomtweets2", "randomtweets3", et "randomtweets4"
sampled_df = df[df["document"].isin(["randomtweets1", "randomtweets2", "randomtweets3", "randomtweets4"])].sample(n=200, random_state=42)

# Demander la polarité pour chaque contenu et l'ajouter à la colonne 'polarite' du DataFrame
for i in sampled_df["content"].index:
    print(f"Contenu: {df['content'][i]}")
    # Demander à l'utilisateur d'entrer la polarité pour chaque contenu
    df.at[i, 'polarite'] = input("Polarité: ")

# Sauvegarder le DataFrame mis à jour avec les polarités dans un nouveau fichier JSON
df.to_json("Corpus/Full_DataSet_positivite.json")

# Séparer les contenus en listes positives et négatives en fonction de la polarité
# '1' pour les contenus positifs et '-1' pour les contenus négatifs
content_positive = df[df['polarite'] == '1']["content_processed"].tolist()
content_negative = df[df['polarite'] == '-1']["content_processed"].tolist()

# Fonction pour compter les mots les plus communs dans une liste de listes de tokens
def most_common_words(token_lists):
    words = []
    for tokens in token_lists:
        # Étendre la liste des mots avec les tokens
        words.extend(tokens)
    # Retourner les 50 mots les plus communs
    return Counter(words).most_common(50)

# Obtenir les mots les plus communs pour les contenus positifs et négatifs
common_words_positive = most_common_words(content_positive)
common_words_negative = most_common_words(content_negative)

# Fichier pour enregistrer les résultats des mots les plus communs dans les contenus positifs
output_file_positive = 'Liste_mots/most_common_words_pos.txt'

# Ouvrir le fichier en mode écriture et écrire les mots les plus communs dans les contenus positifs
with open(output_file_positive, 'w') as f:
    for word, count in common_words_positive:
        f.write(f"{word}: {count}\n")

print(f"Les mots les plus communs positifs ont été enregistrés dans {output_file_positive}.")

# Fichier pour enregistrer les résultats des mots les plus communs dans les contenus négatifs
output_file_negative = 'Liste_mots/most_common_words_neg.txt'

# Ouvrir le fichier en mode écriture et écrire les mots les plus communs dans les contenus négatifs
with open(output_file_negative, 'w') as f:
    for word, count in common_words_negative:
        f.write(f"{word}: {count}\n")

print(f"Les mots les plus communs négatifs ont été enregistrés dans {output_file_negative}.")
