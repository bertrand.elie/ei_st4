from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import BernoulliNB, ComplementNB, MultinomialNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import accuracy_score
import warnings
import pandas as pd
from joblib import dump 

# Chargement des données depuis un fichier JSON
df = pd.read_json("Corpus/Full_DataSet_fr_no_rt_v2.json")

# Prétraitement des données textuelles
# La colonne "content_processed" contient des listes de tokens lemmatisés, on les rejoint en une seule chaîne de caractères
X = df["content_processed"].apply(lambda x: " ".join(x))

# La colonne "comptage_sentiments" contient les étiquettes cibles pour la classification
y = df["comptage_sentiments"]
# Vectorisation TF-IDF
tfidf_vectorizer = TfidfVectorizer()
X_tfidf = tfidf_vectorizer.fit_transform(X)


# Sauvegarder le TfidfVectorizer pour une utilisation ultérieure
dump(tfidf_vectorizer, 'models/tfidf_vectorizer.joblib')

# Séparation des données en ensemble d'entraînement et de test
X_train, X_test, y_train, y_test = train_test_split(X_tfidf, y, test_size=0.2, random_state=42)

# Liste des classificateurs
classifiers = {
    "BernoulliNB": BernoulliNB(),
    "ComplementNB": ComplementNB(),
    "MultinomialNB": MultinomialNB(),
    "KNeighborsClassifier": KNeighborsClassifier(),
    "RandomForestClassifier": RandomForestClassifier(),
    "LogisticRegression": LogisticRegression(max_iter=1000),
    "MLPClassifier": MLPClassifier(max_iter=1000),
    "AdaBoostClassifier": AdaBoostClassifier()
}
# Désactiver les avertissements spécifiques
warnings.filterwarnings('ignore', category=FutureWarning, append=True)

# Entraîner et évaluer chaque classificateur
results = {}
for name, clf in classifiers.items():
    clf.fit(X_train, y_train)
    y_pred = clf.predict(X_test)
    accuracy = accuracy_score(y_test, y_pred)
    results[name] = accuracy
    #Enregistrement des modèles dans des fichiers
    dump(clf,f"models/{name}.joblib")

# Afficher les résultats
sorted_results = sorted(results.items(), key=lambda item: item[1], reverse=True)
for name, accuracy in sorted_results:
    print(f"{accuracy:.2%} - {name}")
    
