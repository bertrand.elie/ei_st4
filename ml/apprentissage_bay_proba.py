import pandas as pd
from collections import defaultdict
import re
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
import json


# Charger le document JSON
json_file_path = "chemin_vers_le_fichier.json"
with open(json_file_path, "r", encoding="utf-8") as file:
    data = json.load(file)

# Extraire les tweets et leurs classes
tweets = data["tweets"][2:5703]  # Les tweets de la 3ème à la 5703ème ligne
# Les classes de la 75057ème à la 86460ème ligne
classes = data["classes"][75056:86460]

# Créer un DataFrame Pandas pour les données
df = pd.DataFrame({"tweet": tweets, "sentiment": classes})

# Charger le corpus CSV
corpus_path = r'C:\Users\Utilisateur\ei_st4\Corpus\CorpusRandomTwitter\randomtweets1.csv'
df = pd.read_csv(corpus_path)

# Remplacer par les noms réels des colonnes pour les tweets et les sentiments
tweet_col = 'x'  # Remplacer par le nom réel de la colonne contenant les tweets
# Remplacer par le nom réel de la colonne contenant les sentiments
sentiment_col = 'sentiments-bertweet'

# Assumer que les sentiments peuvent être 'positif', 'neutre', 'negatif'
CLASSES = ['POS', 'NEU', 'NEG']


def clean_tweet(tweet):
    """Nettoyer le tweet en supprimant les caractères spéciaux et en le mettant en minuscules."""
    tweet = re.sub(r'\W', ' ', tweet)
    tweet = re.sub(r'\s+', ' ', tweet)
    tweet = tweet.lower().strip()
    return tweet


def train_bayes_classifier(df, tweet_col, sentiment_col):
    term_counts = {cls: defaultdict(float) for cls in CLASSES}
    class_counts = defaultdict(int)
    total_terms = {cls: 0 for cls in CLASSES}

    for _, row in df.iterrows():
        tweet = clean_tweet(row[tweet_col])
        sentiment = row[sentiment_col]
        terms = tweet.split()

        class_counts[sentiment] += 1

        for term in terms:
            term_counts[sentiment][term] += 1
            total_terms[sentiment] += 1

    vocab = set(term for cls in CLASSES for term in term_counts[cls])
    term_probs = {cls: defaultdict(float) for cls in CLASSES}
    class_probs = {cls: class_counts[cls] / len(df) for cls in CLASSES}

    for cls in CLASSES:
        for term in vocab:
            term_probs[cls][term] = (
                term_counts[cls][term] + 1) / (total_terms[cls] + len(vocab))  # Laplace smoothing

    return term_probs, class_probs, vocab


def predict(tweet, term_probs, class_probs, vocab):
    tweet = clean_tweet(tweet)
    terms = tweet.split()

    class_scores = {}
    for cls in CLASSES:
        score = class_probs[cls]
        for term in terms:
            if term in vocab:
                score *= term_probs[cls][term]
        class_scores[cls] = score

    return max(class_scores, key=class_scores.get)


# Préparer les données
df[tweet_col] = df[tweet_col].apply(clean_tweet)

# Diviser les données en ensemble d'entraînement et de test
train_df, test_df = train_test_split(df, test_size=0.2, random_state=42)
# Modifier les données de test (exemple de modification)
# Ajoutons un mot supplémentaire à chaque tweet de l'ensemble de test
test_df[tweet_col] = test_df[tweet_col].apply(
    lambda x: x + " Haha la découverte")
# Entraîner le classificateur bayésien
term_probs, class_probs, vocab = train_bayes_classifier(
    train_df, tweet_col, sentiment_col)

# Prédire les sentiments pour l'ensemble de test
test_df['predicted_sentiment'] = test_df[tweet_col].apply(
    lambda x: predict(x, term_probs, class_probs, vocab))


# Évaluer la précision
accuracy = accuracy_score(
    test_df[sentiment_col], test_df['predicted_sentiment'])
print(f"Accuracy: {accuracy:.2f}")

# Afficher quelques prédictions
print(test_df[[tweet_col, sentiment_col, 'predicted_sentiment']].head())


# Garder uniquement les tweets de base qui ont été prédits comme étant positifs ou négatifs
predicted_sentiments = test_df['predicted_sentiment']
filtered_df = df.iloc[test_df.index][predicted_sentiments.isin(['POS', 'NEG'])]
# Garder uniquement les tweets de base qui ont été prédits comme étant positifs ou négatifs
predicted_sentiments = test_df['predicted_sentiment']
filtered_df = df.iloc[test_df.index][predicted_sentiments.isin(['POS', 'NEG'])]

# Afficher les tweets de base prédits comme étant positifs ou négatifs
print(filtered_df[[tweet_col, sentiment_col]])
