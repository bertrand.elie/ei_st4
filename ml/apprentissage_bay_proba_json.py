import json
import pandas as pd
import numpy as np
from collections import defaultdict
import re
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, recall_score, precision_score
import matplotlib.pyplot as plt

# Charger le fichier JSON
json_file_path = r"./Corpus/Full_DataSet_fr_no_rt_v2.json"
with open(json_file_path, 'r', encoding='utf-8') as file:
    data = json.load(file)

# Vérifier la structure des données pour éviter les erreurs de clé dans les dictionnaires
if isinstance(data, dict) and "content_processed" in data:
    for key in data["content_processed"]:
        if isinstance(data["content_processed"][key], list):
            data["content_processed"][key] = ' '.join(
                data["content_processed"][key])
else:
    raise ValueError(
        "La structure des données est incorrecte ou 'content_processed' n'est pas présent")

# Extraire les tweets et les sentiments des données JSON en s'assurant que notre base de donné n'avait pas de manquement
tweets = [data["content_processed"][key] for key in data["content_processed"]]
classes = [data["sentiments-bertweet"][key]
           for key in data["sentiments-bertweet"]]


# Créer un DataFrame Pandas pour les données
df = pd.DataFrame({"tweet": tweets, "sentiment": classes})

# Remplacer par les noms réels des colonnes pour les tweets et les sentiments
tweet_col = 'tweet'  # Nom réel de la colonne contenant les tweets
sentiment_col = 'sentiment'  # Nom réel de la colonne contenant les sentiments
# Définire les classe (-1: négatif, 0:neutre et 1:positif)
CLASSES = [-1, 0, 1]

# cette fonction sera pour prédire les tweets et s'assurer qu'ils sont comparable à notre base de donnée


def clean_tweet(tweet):
    """Nettoyer le tweet en supprimant les caractères spéciaux et en le mettant en minuscules."""
    tweet = re.sub(r'\W', ' ', tweet)
    tweet = re.sub(r'\s+', ' ', tweet)
    tweet = tweet.lower().strip()
    return tweet

# fonction d'apprentissage


def train_bayes_classifier(df, tweet_col, sentiment_col):
    # intialisation d'un dictionnaire pour dénombrer la répétition de chaque terme dans chaque classe
    term_counts = {cls: defaultdict(float) for cls in CLASSES}
    # initialisation d'un dictionnaire pour dénombrer le nombre de tweet dans dans chaque classes
    class_counts = defaultdict(int)
    # initialisation d'un dictionnaire pour dénombrer le nombre de mot(terme) pour chaque classe
    total_terms = {cls: 0 for cls in CLASSES}

    for _, row in df.iterrows():  # on itère sur chaque tweet
        tweet = row[tweet_col]
        sentiment = row[sentiment_col]
        terms = tweet.split()  # on met les tweet sous forme de liste de mot

        # on incrémente le dictionnaire pour dénombrer le nombre de tweet par sentiment
        class_counts[sentiment] += 1

        for term in terms:  # on itère sur chaque terme des tweet et on incrémente les dictionnaires
            term_counts[sentiment][term] += 1
            total_terms[sentiment] += 1

    # on créé notre vocabulaire de nos données avec une seule fois chaque terme grace à la structure d'enssemble
    vocab = set(term for cls in CLASSES for term in term_counts[cls])
    # initialisation d'un dictionnaire pour la probabilité de chaque sachant la présence du terme
    term_probs = {cls: defaultdict(float) for cls in CLASSES}
    # dictionnaire de la probabilité de chaque classe
    class_probs = {cls: class_counts[cls] / len(df) for cls in CLASSES}

    for cls in CLASSES:
        for term in vocab:  # on itère pour chaque classe sur tous les termes du vocabulaire
            term_probs[cls][term] = (
                term_counts[cls][term]+1) / (total_terms[cls]+len(vocab))  # calcul de la probabilité de la classe du tweet sachant la présence du terme, on calcul en faisant un lissage et éviter les valeurs à 0

    return term_probs, class_probs, vocab

# fonction de prédicition


def predict(tweet, term_probs, class_probs, vocab):
    # on s'assure que les tweet sont comparable à notre base de donnés
    tweet = clean_tweet(tweet)
    terms = tweet.split()  # on met les tweet sous forme de liste de mots
    # Initialiser avec les log probabilités des classes
    # on prend le le logarithme de la probabilité de chaque classe pour calculer le score du tweet
    class_scores = {cls: np.log(class_probs[cls]) for cls in CLASSES}

    for term in terms:
        if term in vocab:
            for cls in CLASSES:
                # on calcule le score du tweet avec tous les termes présent dans le tweet et la base de donnée
                class_scores[cls] += np.log(term_probs[cls][term])
    # on renvoie la classe qui maximise le socre pour le tweeet
    return max(class_scores, key=class_scores.get)


# Préparer les données

mid_point = len(df)//13

# division entre la base de tests et celle d'apprentissage
test_df = df.iloc[:mid_point]
train_df = df.iloc[mid_point:]

# Entraîner le classificateur bayésien
term_probs, class_probs, vocab = train_bayes_classifier(
    train_df, tweet_col, sentiment_col)  # entrainement du modèle

# Prédire les sentiments pour l'ensemble de test
test_df['predicted_sentiment'] = test_df[tweet_col].apply(
    lambda x: predict(x, term_probs, class_probs, vocab))

# Calculer le rappel et la précision pour les données négatives (sentiment_roberta=-1)
recall_neg = recall_score(
    test_df[test_df[sentiment_col] == -1][sentiment_col], test_df[test_df[sentiment_col] == -1]['predicted_sentiment'], average='macro')
precision_neg = precision_score(
    test_df[test_df[sentiment_col] == -1][sentiment_col], test_df[test_df[sentiment_col] == -1]['predicted_sentiment'], average='macro')

# Calculer le rappel et la précision pour les données positives (sentiment_roberta=1)
recall_pos = recall_score(
    test_df[test_df[sentiment_col] == 1][sentiment_col], test_df[test_df[sentiment_col] == 1]['predicted_sentiment'], average='macro')
precision_pos = precision_score(
    test_df[test_df[sentiment_col] == 1][sentiment_col], test_df[test_df[sentiment_col] == 1]['predicted_sentiment'], average='macro')

# Calculer le rappel et la précision pour les données positives (sentiment_roberta=0)
recall_neu = recall_score(
    test_df[test_df[sentiment_col] == 0][sentiment_col], test_df[test_df[sentiment_col] == 0]['predicted_sentiment'], average='macro')
precision_neu = precision_score(
    test_df[test_df[sentiment_col] == 0][sentiment_col], test_df[test_df[sentiment_col] == 0]['predicted_sentiment'], average='macro')

# Évaluer la précision
accuracy = accuracy_score(
    test_df[sentiment_col], test_df['predicted_sentiment'])

# fonction pour mettre sous la forme qui convient l'enssemble des tweets scrapés


def read_tweets_from_file(file_path):
    tweets = []
    with open(file_path, 'r', encoding='utf-8') as file:
        lines = file.readlines()  # lire le fichier ligne par ligne
        for line in lines:
            tweet = line.strip().split(', ')[-1].strip(" '")
            tweets.append(tweet)  # mis sous la bonne forme du tweet
    return tweets

# Prédire les sentiments pour les tweets les tweets scrapés


def predict_tweets_from_file(file_path, term_probs, class_probs, vocab):
    tweets = read_tweets_from_file(file_path)
    cleaned_tweets = [clean_tweet(tweet) for tweet in tweets]
    predictions = [predict(tweet, term_probs, class_probs, vocab)
                   for tweet in cleaned_tweets]

    for tweet, prediction in zip(tweets, predictions):
        # affichage des prédiction des tweets scrapés
        print(f"Tweet: {tweet}\nPredicted Sentiment: {prediction}\n")


# Chemin vers le fichier texte contenant les tweets
text_file_path = r"./scraped_data.txt"  # Remplacez par le chemin réel

# Prédire les sentiments pour les tweets du fichier texte
predict_tweets_from_file(text_file_path, term_probs, class_probs, vocab)

# affichage des métrique d'évaluation
print(f"Accuracy: {accuracy:.2f}")
print("Recall Positives:", recall_pos)
print("Precision Positives:", precision_pos)
print("Recall Negatives:", recall_neg)
print("Precision Negatives:", precision_neg)
print("Recall neutre:", recall_neu)
print("Precision neutre:", precision_neu)
