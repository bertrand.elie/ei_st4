import torch
import pandas as pd
import torch.nn as nn
import torch.optim as optim
import numpy as np
from collections import Counter
import random

dataset=pd.read_json("Corpus/Full_DataSet_fr_no_rt_v2.json")

#Exemple de liste de tokens
comments = list(dataset["content_processed"])




# Construire un vocabulaire
tokens = [word for comment in comments for word in comment]
vocab = list(set(tokens))
word_to_idx = {word: i for i, word in enumerate(vocab)}
idx_to_word = {i: word for i, word in enumerate(vocab)}

# Paramètres
window_size = 2
embedding_dim = 10
batch_size = 64
negative_samples = 5

# Générer des paires (contexte, cible)
def generate_context_target_pairs(comments, window_size):
    pairs = []
    for comment in comments:
        for i, word in enumerate(comment):
            start = max(0, i - window_size)
            end = min(len(comment), i + window_size + 1)
            context_words = [comment[j] for j in range(start, end) if j != i]
            for context_word in context_words:
                pairs.append((word, context_word))
    return pairs

pairs = generate_context_target_pairs(comments, window_size)
pair_indices = [(word_to_idx[pair[0]], word_to_idx[pair[1]]) for pair in pairs]

# Modèle Word2Vec avec échantillonnage négatif
class Word2VecNeg(nn.Module):
    def __init__(self, vocab_size, embedding_dim):
        super(Word2VecNeg, self).__init__()
        self.embeddings = nn.Embedding(vocab_size, embedding_dim)
        self.context_embeddings = nn.Embedding(vocab_size, embedding_dim)
        
    def forward(self, pos_pairs, neg_pairs):
        pos_u = self.embeddings(pos_pairs[:, 0])
        pos_v = self.context_embeddings(pos_pairs[:, 1])
        pos_score = torch.sum(pos_u * pos_v, dim=1)
        pos_score = torch.nn.functional.logsigmoid(pos_score)
        
        neg_u = self.embeddings(neg_pairs[:, 0])
        neg_v = self.context_embeddings(neg_pairs[:, 1])
        neg_score = torch.sum(neg_u * neg_v, dim=1)
        neg_score = torch.nn.functional.logsigmoid(-neg_score)
        
        return -torch.sum(pos_score) - torch.sum(neg_score)

# Initialisation du modèle
vocab_size = len(vocab)
model = Word2VecNeg(vocab_size, embedding_dim)
optimizer = torch.optim.Adam(model.parameters(), lr=0.01)

# Générer des échantillons négatifs
def get_negative_samples(target_idx, vocab_size, num_samples):
    neg_samples = []
    while len(neg_samples) < num_samples:
        sample = random.randint(0, vocab_size - 1)
        if sample != target_idx:
            neg_samples.append(sample)
    return neg_samples

# Convertir les paires en indices et générer des mini-lots
def get_batches(pairs, batch_size, vocab_size, negative_samples):
    random.shuffle(pairs)
    batches = []
    for i in range(0, len(pairs), batch_size):
        batch = pairs[i:i + batch_size]
        pos_pairs = torch.tensor(batch, dtype=torch.long)
        neg_pairs = []
        for pair in batch:
            target = pair[1]
            neg_samples = get_negative_samples(target, vocab_size, negative_samples)
            for neg in neg_samples:
                neg_pairs.append((pair[0], neg))
        neg_pairs = torch.tensor(neg_pairs, dtype=torch.long)
        batches.append((pos_pairs, neg_pairs))
    return batches

# Entraînement du modèle
n_epochs = 100
for epoch in range(n_epochs):
    total_loss = 0
    batches = get_batches(pair_indices, batch_size, vocab_size, negative_samples)
    for pos_pairs, neg_pairs in batches:
        model.zero_grad()
        loss = model(pos_pairs, neg_pairs)
        loss.backward()
        optimizer.step()
        total_loss += loss.item()
    print(f"Epoch {epoch + 1}, Loss: {total_loss}")

print("Entraînement terminé.")

# Extraction des vecteurs de mots
word_vectors = {}
for word, idx in word_to_idx.items():
    word_vectors[word] = model.embeddings.weight.data[idx].numpy()

# Affichage des vecteurs
for word, vector in word_vectors.items():
    print(f"{word}: {vector}")

# Le dictionnaire final contenant les vecteurs de mots
print(word_vectors)
